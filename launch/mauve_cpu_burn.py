import lttngust
import logging

logging.basicConfig()
lttng = logging.getLogger("mauve")

import mauve_runtime
from rospkg import RosPack
rp = RosPack()
p = rp.get_path("benchmarking")
mauve_runtime.load_deployer(p + "/../../devel/lib/libmauve_cpu_architecture.so")
depl = mauve_runtime.deployer

mauve_runtime.initialize_logger(b'''
default:
  level: info
  type: stdout
runtime:
  - level: debug
    type: stdout
''')

depl.architecture.configure()
depl.create_tasks()
depl.activate()

lttng.info("start")

depl.start()
