#include <sched.h>
#include <pthread.h>
#include <iostream>
#include <cstring>

#include "benchmarking/cpu.hpp"

namespace benchmarking {

uint64_t total_usage(const struct rusage& usage) {
  uint64_t u_sec = usage.ru_utime.tv_sec;
  uint64_t u_micro = usage.ru_utime.tv_usec;
  uint64_t s_sec = usage.ru_stime.tv_sec;
  uint64_t s_micro = usage.ru_stime.tv_usec;

  return (u_sec + s_sec) * 1000000 + (u_micro + s_micro);
}

uint64_t burn_cpu(uint64_t microsec){
  struct rusage usage;
  getrusage(RUSAGE_THREAD, &usage);
  uint64_t init = total_usage(usage);
  uint64_t duration = 0;
  while(duration < microsec) {
    getrusage(RUSAGE_THREAD, &usage);
    duration = total_usage(usage) - init;
  }
  return duration;
}

int stick_to_core(int core_id) {
  pthread_t thread;
  thread = pthread_self();
  cpu_set_t cpuset;
  CPU_ZERO(&cpuset);
  CPU_SET(core_id, &cpuset);
  return pthread_setaffinity_np(thread, sizeof(cpu_set_t), &cpuset);
}

int set_rt_prio(int priority) {
  sched_param sch_params;
  sch_params.sched_priority = priority;
  return pthread_setschedparam(0, SCHED_FIFO, &sch_params);
}

}
