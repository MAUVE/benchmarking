#include <ros/ros.h>

#include <mauve/tracing.hpp>

#include "benchmarking/cpu.hpp"

int main(int argc, char** argv) {

  ros::init(argc, argv, "cpu");

  uint64_t compute_time = 1000 * 1000;
  uint64_t frequency = 1;
  int core = 0;

  ros::NodeHandle n("~");

  double d;
  if (n.getParam("C", d)) {
    compute_time = compute_time * d;
    ROS_INFO_STREAM("C: " << compute_time);
  } else ROS_ERROR("No C specified");

  if (n.getParam("P", d)) {
    frequency = 1 / d;
    ROS_INFO_STREAM("freq: " << frequency);
  } else ROS_ERROR("No P specified");

  if (n.getParam("cpu", core)) {
    ROS_INFO_STREAM("cpu: " << core);
  } else ROS_ERROR("No cpu specified");

  benchmarking::stick_to_core(core);

  mauve::tracing::trace_event(ros::this_node::getName());
  ros::Rate loop_rate(frequency);

  while (ros::ok()) {
    mauve::tracing::trace_start(ros::this_node::getName());
    benchmarking::burn_cpu(compute_time);
    ros::spinOnce();
    mauve::tracing::trace_end(ros::this_node::getName());
    loop_rate.sleep();
  }

  return 0;
}
