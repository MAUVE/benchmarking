#include <rtt/RTT.hpp>
#include <rtt/Component.hpp>
#include <mauve/tracing.hpp>
#include <string>

#include "benchmarking/cpu.hpp"

namespace benchmarking {

class CpuBurn: public RTT::TaskContext {
public:
  CpuBurn(const std::string& name)
  : RTT::TaskContext(name)
  {
    this->addProperty("C", compute_time).doc("capacity");
  }

  bool startHook() {
    mauve::tracing::trace_event(this->getName());
    return true;
  }

  void updateHook() {
    mauve::tracing::trace_start(this->getName());
    uint64_t c = compute_time * 1000 * 1000;
    benchmarking::burn_cpu(c);
    mauve::tracing::trace_end(this->getName());
  }

private:
  double compute_time;
};

}

ORO_CREATE_COMPONENT( benchmarking::CpuBurn );
