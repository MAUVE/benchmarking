#include <mauve/runtime.hpp>
#include <mauve/tracing.hpp>
#include "benchmarking/mauve_component.hpp"

namespace benchmarking {

struct CpuBurnArchitecture: public mauve::runtime::Architecture {
  using CpuBurn = mauve::runtime::Component<mauve::runtime::Shell,
    CpuBurnCore, CpuBurnFSM>;

  CpuBurn& A = mk_component<CpuBurn>("/A");
  CpuBurn& B = mk_component<CpuBurn>("/B");

  virtual bool configure_hook() override {
    A.fsm().sleep.set_clock(mauve::runtime::ms_to_ns(100));
    B.fsm().sleep.set_clock(mauve::runtime::ms_to_ns(100));
    A.set_cpu(1);
    B.set_cpu(1);
    A.set_priority(20);
    B.set_priority(30);
    A.core().C = 0.02;
    B.core().C = 0.01;
    A.configure();
    B.configure();
    return true;
  }
};

extern "C" void mk_python_deployer() {
  mk_abstract_deployer(new CpuBurnArchitecture());
}

}
