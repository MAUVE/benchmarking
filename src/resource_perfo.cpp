#include <iostream>

#include <mauve/runtime.hpp>
#include <mauve/tracing.hpp>

using namespace mauve::runtime;

#define DATA_SIZE 10000

struct DataType {
  uint8_t tab[DATA_SIZE];
};
std::ostream& operator<<(std::ostream & out, const DataType& value) {
  out << "[";
  for (int i = 0; i < DATA_SIZE; i++)
    out << value.tab[i] << ", ";
  out << "]";
  return out;
};

struct S: public Shell {
  WritePort<DataType> & out = mk_write_port<DataType>("out");
  ReadPort<DataType>  & in  = mk_read_port<DataType>("in", DataType());
};

struct C: public Core<S> {

  bool configure_hook() override {
    for (int i = 0; i < DATA_SIZE; ++i) {
      data.tab[i] = i;
    }
    return true;
  }

  void write() {
    std::cout << "write" << std::endl;
    std::cout.flush();
    mauve::tracing::trace_start("write");
    shell().out.write(data);
    mauve::tracing::trace_end("write");
  }

  void read() {
    std::cout << "read" << std::endl;
    std::cout.flush();
    mauve::tracing::trace_start("read");
    data = shell().in.read();
    mauve::tracing::trace_end("read");
  }

private:
  DataType data;
};

struct FSM: public FiniteStateMachine<S, C> {
  ExecState<C>    & W  = mk_execution("Write", &C::write);
  SynchroState<C> & S1 = mk_synchronization("S1", ms_to_ns(10));
  ExecState<C>    & R  = mk_execution("Read", &C::read);
  SynchroState<C> & S2 = mk_synchronization("S2", ms_to_ns(10));

  bool configure_hook() override {
    set_initial(W);
    set_next(W, S1);
    set_next(S1, R);
    set_next(R, S2);
    set_next(S2, W);
    return true;
  }
};

struct A: public Architecture {
  Component<S, C, FSM> & cpt = mk_component<S, C, FSM>("cpt");
  SharedData<DataType> & res = mk_resource<SharedData<DataType>>("res", DataType());

  bool configure_hook() override {
    cpt.shell().out.connect(res.interface().write);
    cpt.shell().in.connect(res.interface().read_value);


    cpt.set_cpu(1);
    cpt.set_priority(10);

    return Architecture::configure_hook();
  }
};

int main(int argc, char const *argv[]) {
  // Create the architecture
  A architecture;

  AbstractDeployer* deployer = mk_abstract_deployer(&architecture);

  architecture.configure();

  deployer->create_tasks();
  deployer->activate();

  deployer->start();

  deployer->loop();

  deployer->stop();

  return 0;
}
