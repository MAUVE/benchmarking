#include <cstdlib>
#include <mauve/runtime.hpp>
#include <mauve/tracing.hpp>
#include "benchmarking/mauve_component.hpp"

namespace benchmarking {

struct CpuBurnArchitecture: public mauve::runtime::Architecture {
  using CpuBurn = mauve::runtime::Component<mauve::runtime::Shell,
    CpuBurnCore, CpuBurnFSM>;

  CpuBurn& A = mk_component<CpuBurn>("disturbing");
};

}

int main() {
  std::stringstream config;
  config << "default:" << std::endl;
  config << "  level: warn" << std::endl;
  config << "  type: stdout" << std::endl;
  /*config << "disturbing:" << std::endl;
  config << "  - type: stdout" << std::endl;
  config << "    level: debug" << std::endl;*/
  mauve::runtime::AbstractLogger::initialize(config);
  auto* a = new benchmarking::CpuBurnArchitecture();
  auto* d = mk_deployer(a);
  a->A.set_cpu(1);
  a->A.set_priority(10);
  a->configure();
  d->create_tasks();
  d->activate();
  d->start();
  d->loop();
  d->stop();
  a->cleanup();
  return 0;
}
