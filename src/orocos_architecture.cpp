#include <rtt/RTT.hpp>
#include <rtt/Component.hpp>
#include <ocl/TaskBrowser.hpp>
#include <mauve/tracing.hpp>
#include <string>

#include "benchmarking/cpu.hpp"

namespace benchmarking {

class CpuBurn: public RTT::TaskContext {
public:
  CpuBurn(const std::string& name)
  : RTT::TaskContext(name)
  {
    this->addProperty("C", compute_time).doc("capacity");
  }

  bool startHook() {
    mauve::tracing::trace_event(this->getName());
    return true;
  }

  void updateHook() {
    mauve::tracing::trace_start(this->getName());
    uint64_t c = compute_time * 1000 * 1000;
    benchmarking::burn_cpu(c);
    mauve::tracing::trace_end(this->getName());
  }

private:
  double compute_time;
};

}

#include <rtt/os/main.h>

using namespace RTT;
using namespace benchmarking;

int ORO_main(int argc, char** argv) {
  log().setLogLevel(Logger::Info);

  stick_to_core(2);
  //set_rt_prio(35);

  CpuBurn A("/A");
  CpuBurn B("/B");
  A.setActivity( new Activity(ORO_SCHED_RT, 20, 0.1) );
  B.setActivity( new Activity(ORO_SCHED_RT, 30, 0.1) );
  A.setCpuAffinity(2);
  dynamic_cast<Property<double>*>(A.getProperty("C"))->set(0.02);
  B.setCpuAffinity(2);
  dynamic_cast<Property<double>*>(B.getProperty("C"))->set(0.01);

  A.configure();
  B.configure();

  A.start();
  B.start();

  OCL::TaskBrowser b( &A );
  b.loop();

  return 0;
}
