#ifndef MAUVE_BENCHMARKING_CPU_H
#define MAUVE_BENCHMARKING_CPU_H

#include <iostream>
#include <cstdint>
#include <sys/time.h>
#include <sys/resource.h>

namespace benchmarking {

uint64_t total_usage(const struct rusage& usage);

uint64_t burn_cpu(uint64_t microsec);

int stick_to_core(int core_id);

int set_rt_prio(int priority);

}

#endif
