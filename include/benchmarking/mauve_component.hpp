#ifndef BENCHMARKING_MAUVE_COMPONENT_HPP
#define BENCHMARKING_MAUVE_COMPONENT_HPP

#include <mauve/runtime.hpp>
#include <mauve/tracing.hpp>
#include "benchmarking/cpu.hpp"

namespace benchmarking {

struct CpuBurnCore: public mauve::runtime::Core<mauve::runtime::Shell> {
  mauve::runtime::Property<double>& C = mk_property("C", -1.0);
  mauve::runtime::Property<double>& config_time = mk_property("config_time", 0.0);
  virtual bool configure_hook() override {
    srand (time(NULL));
    logger().info("configure during {}", (config_time*1000*1000));
    benchmarking::burn_cpu(config_time * 1000 * 1000);
    return true;
  };
  virtual void update() {
    mauve::tracing::trace_start(this->container_name());
    uint64_t c = (C > 0) ? C * 1000 * 1000 : (1 + rand() % 4) * 1000;
    logger().debug("{}", c);
    benchmarking::burn_cpu(c);
    mauve::tracing::trace_end(this->container_name());
  };
};

struct CpuBurnFSM: public mauve::runtime::FiniteStateMachine<
    mauve::runtime::Shell,CpuBurnCore> {

  mauve::runtime::ExecState<CpuBurnCore>& burn = mk_execution("burn", &CpuBurnCore::update);
  mauve::runtime::SynchroState<CpuBurnCore>& sleep = mk_synchronization("sleep",
    mauve::runtime::sec_to_ns(1));

  bool configure_hook() {
    set_initial(burn);
    set_next(burn, sleep);
    set_next(sleep, burn);
    return true;
  }
};

}

#endif
