#! /usr/bin/env python3

try:
    from babeltrace.reader import TraceCollection
except:
    from babeltrace import TraceCollection
import statistics

def ns_to_ms(ns):
    return ns / 1000 / 1000

class Statistics:
    def __init__(self):
        self.trace_collection = TraceCollection()
        self.T0 = 0

    def load_trace(self, trace_path, max_count=0):
        self.trace_collection.add_traces_recursive(trace_path, 'ctf')

    def analyse(self):
        starts = []
        releases = []
        reconf_durations = []
        r = None
        for e in self.trace_collection.events:
            try:
                if ("component_start" in e.name) and (e["name"] == "/A"):
                    starts.append(ns_to_ms(e.timestamp))
                if "start" in e.name:
                    if e["message"] == "/A":
                        if not (r is None):
                            releases.append(ns_to_ms(e.timestamp) - last_r)
                            r = None
                        last_r = ns_to_ms(e.timestamp)
                    elif e["message"] == "reconfiguration":
                        r = ns_to_ms(e.timestamp)
                if "end" in e.name and e["message"] == "reconfiguration":
                    reconf_durations.append(ns_to_ms(e.timestamp) - r)
            except KeyError:
                pass

        return statistics.mean(reconf_durations), statistics.stdev(reconf_durations), statistics.mean(releases), statistics.stdev(releases)

if __name__ == "__main__":

    import argparse
    parser = argparse.ArgumentParser(description='Compute periodicity of components from a trace')
    parser.add_argument('trace', type=str, help='path to trace folder')
    args = parser.parse_args()

    trace_path = args.trace

    stats = Statistics()
    stats.load_trace(trace_path)
    r = stats.analyse()

    print(r)
