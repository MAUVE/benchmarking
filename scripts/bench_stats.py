#! /usr/bin/env python3

try:
    from babeltrace.reader import TraceCollection
except:
    from babeltrace import TraceCollection
import statistics

def ns_to_ms(ns):
    return ns / 1000 / 1000

class Statistics:
    def __init__(self):
        self.trace_collection = TraceCollection()
        self.T0 = 0

    def load_trace(self, trace_path, max_count=0):
        self.trace_collection.add_traces_recursive(trace_path, 'ctf')

    def analyse(self):
        results = {'/A': {}, '/B': {}}

        T0 = 0
        T = {'/A': 0, '/B': 0}
        starts = {'/A': [], '/B': []}
        durations = {'/A': [], '/B': []}
        for e in self.trace_collection.events:
            try:
                if ("component_start" in e.name) and (e["name"] in results.keys()):
                    if T0 == 0: T0 = ns_to_ms(e.timestamp)
                    T[e["name"]] = ns_to_ms(e.timestamp)
                    T[e["name"]] = ns_to_ms(e.timestamp)
                comp = e["message"]
                if "start" in e.name:
                    starts[comp].append(ns_to_ms(e.timestamp))
                if "end" in e.name:
                    d = ns_to_ms(e.timestamp) - starts[comp][-1]
                    durations[comp].append(d)
                if ("event" in e.name) and (e["message"] in results.keys()):
                    if T[comp] == 0: T[comp] = ns_to_ms(e.timestamp)
                    if T0 == 0: T0 = ns_to_ms(e.timestamp)
            except KeyError:
                pass

        for c in results.keys():
            results[c]["N"] = len(starts[c])
            results[c]["S0"] = (starts[c][0] - T[c])
            results[c]["R0"] = (starts[c][0] + durations[c][0] - T[c])
            results[c]["mean(E)"] = statistics.mean(durations[c])
            results[c]["min(E)"] = min(durations[c])
            results[c]["max(E)"] = max(durations[c])
            results[c]["stdev(E)"] = statistics.stdev(durations[c])
            l = [ (b-a) for (a,b) in zip(starts[c], starts[c][1:]) ]
            results[c]["mean(T)"] = statistics.mean(l)
            results[c]["min(T)"] = min(l)
            results[c]["max(T)"] = max(l)
            results[c]["stdev(T)"] = statistics.stdev(l)

        return results

if __name__ == "__main__":

    import argparse
    parser = argparse.ArgumentParser(description='Compute periodicity of components from a trace')
    parser.add_argument('trace', type=str, help='path to trace folder')
    args = parser.parse_args()

    trace_path = args.trace

    stats = Statistics()
    stats.load_trace(trace_path)
    r = stats.analyse()

    print("{} {} {} {} {} {} {} {} {} {} {} {} {} {} {} {} {} {} {} {} {}".format(
        r['/A']['N'],
        r['/A']['S0'],
        r['/A']['R0'],
        r['/A']['mean(E)'],
        r['/A']['min(E)'],
        r['/A']['max(E)'],
        r['/A']['stdev(E)'],
        r['/A']['mean(T)'],
        r['/A']['min(T)'],
        r['/A']['max(T)'],
        r['/A']['stdev(T)'],
        r['/B']['S0'],
        r['/B']['R0'],
        r['/B']['mean(E)'],
        r['/B']['min(E)'],
        r['/B']['max(E)'],
        r['/B']['stdev(E)'],
        r['/B']['mean(T)'],
        r['/B']['min(T)'],
        r['/B']['max(T)'],
        r['/B']['stdev(T)']
    ))
